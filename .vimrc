if has('gui_running')
colorscheme torte
endif


"WINDOWS settings"
if has('win32')
"Standart vimrc"
set nocompatible
source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
behave mswin

set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction
"\Standart vimrc"


"Move Backup to Temp
set backup
set backupdir=$HOME\.vim\tmp 
set backupskip=$HOME\.vim\tmp\*
set directory=$HOME\.vim\tmp
set writebackup
endif

"Pathogen
execute pathogen#infect()
syntax on
filetype plugin indent on

"Local pwd for each file
cd $HOME
autocmd BufEnter * silent! lcd %:p:h

set number
set autoindent
set vb
set scrolloff=8
map <F2> :Project<CR>

set omnifunc=syntaxcomplete#Complete
