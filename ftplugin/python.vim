"Keymappings
map <buffer> <F5> Y<ESC>:py3 <C-R>"
map <buffer> <F6> <ESC>:w<CR> :py3f %<CR>

"Fix 'Not Saved'-Bullshit"
set hidden

"Format"
setlocal tabstop=4
setlocal softtabstop=4
setlocal shiftwidth=4
setlocal textwidth=80
setlocal smarttab
setlocal expandtab

set omnifunc=pythoncomplete#Complete
